var oper = "";

function numero(n) {
  var d = document.getElementById("display").value;
  if (d == "0") {
    document.getElementById("display").value = n;
  } else {
    document.getElementById("display").value += n;
  }
}

function parseDisplay(oper) {
  var d = document.getElementById("display").value;
  var op = d.split(oper);
  if (oper == "-") {
    if (op.length == 3) {
      if (op[0] == "") {
        if (op[2] !== "") {
          op = ["-" + op[1], op[2]];
        } else {
          op = ["-" + op[1], ""];
        }
      }
    }
  }
  return op;
}

function calc(oper, op1, op2) {
  var result = 0;
  switch (oper) {
    case "+":
      result = parseInt(op1) + parseInt(op2);
      break;
    case "-":
      result = parseInt(op1) - parseInt(op2);
      break;
    case "*":
      result = parseInt(op1) * parseInt(op2);
      break;
    case "/":
      result = Math.trunc(parseInt(op1) / parseInt(op2));
      if (result == NaN || result == Infinity) {
        result = "Error";
      }
      break;
  }
  return result;
}

function doOperations() {
  var op = parseDisplay(oper);
  switch (oper) {
    case "+":
      if (op[1] == "") {
        document.getElementById("display").value = calc(oper, op[0], op[0]);
      } else {
        document.getElementById("display").value = calc(oper, op[0], op[1]);
      }
      break;
    case "-":
      if (op[1] == "") {
        document.getElementById("display").value = calc(oper, op[0], op[0]);
      } else {
        document.getElementById("display").value = calc(oper, op[0], op[1]);
      }
      break;
    case "*":
      if (op[1] == "") {
        document.getElementById("display").value = calc(oper, op[0], op[0]);
      } else {
        document.getElementById("display").value = calc(oper, op[0], op[1]);
      }
      break;
    case "/":
      if (op[1] == "") {
        document.getElementById("display").value = calc(oper, op[0], op[0]);
      } else {
        document.getElementById("display").value = calc(oper, op[0], op[1]);
      }
      break;
  }
  oper = "";
}

function operador(op) {
  var d = document.getElementById("display").value;
  switch (op) {
    case "+":
      if (oper == "") {
        oper = "+";
        document.getElementById("display").value += oper;
      }
      break;
    case "-":
      if (d == "0" || d == "-") {
        document.getElementById("display").value = "-";
      } else {
        if (oper == "") {
          oper = "-";
          document.getElementById("display").value += oper;
        }
      }
      break;
    case "*":
      if (oper == "") {
        oper = "*";
        document.getElementById("display").value += oper;
      }
      break;
    case "/":
      if (oper == "") {
        oper = "/";
        document.getElementById("display").value += oper;
      }
      break;
    case "c":
      oper = "";
      document.getElementById("display").value = 0;
      break;
    case "=":
      doOperations();
      break;
  }
}
